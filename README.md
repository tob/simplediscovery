```
                            _               _                                                   
       _                   (_ )            ( ) _                                                
  ___ (_)  ___ ___   _ _    | |    __     _| |(_)  ___    ___    _    _   _    __   _ __  _   _ 
/',__)| |/' _ ` _ `\( '_`\  | |  /'__`\ /'_` || |/',__) /'___) /'_`\ ( ) ( ) /'__`\( '__)( ) ( )
\__, \| || ( ) ( ) || (_) ) | | (  ___/( (_| || |\__, \( (___ ( (_) )| \_/ |(  ___/| |   | (_) |
(____/(_)(_) (_) (_)| ,__/'(___)`\____)`\__,_)(_)(____/`\____)`\___/'`\___/'`\____)(_)   `\__, |
                    | |                                                                  ( )_| |
                    (_)                                                                  `\___/'
```
Collects information about the network topology with [OpenWrt](https://openwrt.org/).
It uses the [NetJSON](https://netjson.org) [NetworkGraph format](https://netjson.org/rfc.html#section-4).

### How to install simplediscovery ?

1. Add `src-git simplediscovery https://codeberg.org/tob/simplediscovery.git` to your [feeds.conf](https://openwrt.org/docs/guide-developer/feeds)
2. Run `./scripts/feeds update simplediscovery`
3. Run `./scripts/feeds install simplediscovery`
4. Run `make menuconfig` and select simplediscovery under network
5. Run `make package/simplediscovery/compile` to create only the simplediscovery package or `make` to create a new image

To make Simplediscovery available via [ubus](https://openwrt.org/docs/techref/ubus), it uses [rpcd's](https://openwrt.org/docs/techref/rpcd) plugin executables functionality. The executable is stored in `/usr/libexec/rpcd/simplediscovery`.

### How to use simplediscovery ?

Call simplediscovery with `ubus call simplediscovery get`

Sample output looks like this:
```
{
	"type": "NetworkGraph",
	"protocol": "static",
	"version": "null",
	"metric": "null",
	"router_id": "192.168.4.1"
	"nodes": [
		{
			"id": "192.168.4.188",
			"label": "AP_Basement",
			"properties": {
				"hostname": "AP_Basement",
				"is_router": "1",
				"is_gateway": "0",
				"mac": "f4:60:e2:bf:ca:ed",
				"vendor": "D-Link International"
			}
		},
		{
			"id": "192.168.4.1",
			"label": "node_4",
			"properties": {
				"hostname": "node_4",
				"is_router": "1",
				"is_gateway": "1",
				"mac": "f4:60:e2:bf:c0:b4",
				"vendor": "D-Link International"
			}
		},
		{
			"id": "192.168.4.248",
			"label": "node-6",
			"properties": {
				"hostname": "node-6",
				"is_router": "1",
				"is_gateway": "0",
				"mac": "f4:60:e2:bf:a1:34",
				"vendor": "D-Link International"
			}
		}
	],
	"links": [
		{
			"source": "localhost",
			"target": "192.168.4.188",
			"properties": {
				"target_is_dhcp_client": "1",
				"is_wifi_link": "0",
				"source_interface_name": "eth0",
				"vlan": "0"
			}
		},
		{
			"source": "localhost",
			"target": "192.168.4.248",
			"properties": {
				"target_is_dhcp_client": "1",
				"is_wifi_link": "0",
				"source_interface_name": "eth0",
				"vlan": "0"
			}
		}
	]
}
```
